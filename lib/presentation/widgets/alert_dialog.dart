import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Future alertDialog({bool? isConnection, required BuildContext context}) {
  if (!Platform.isIOS) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        actions: [
          TextButton(
              onPressed: () {
                AppSettings.openWIFISettings();
              },
              child: const Text(
                  'Wifi Connection is down, please check the wifi connection'))
        ],
        title: const Text(
          'Connection error',
          style: TextStyle(
            color: Colors.red,
          ),
        ),
        content: const Text(
            'Seems like your wifi is down \nor you are connected to the wrong IP address'),
      ),
    );
  }
  return showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
            title: const Text(
              'Connection error',
              style: TextStyle(
                color: Colors.red,
              ),
            ),
            content: const Text(
                'Wifi Connection is down, please check the wifi connection'),
            actions: [
              CupertinoDialogAction(
                  onPressed: () => AppSettings.openWIFISettings(),
                  child: const Text('check connection'))
            ],
          ));
}
