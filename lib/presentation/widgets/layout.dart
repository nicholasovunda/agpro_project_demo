import 'dart:async';

import 'package:agpro_project/presentation/widgets/alert_dialog.dart';
import 'package:agpro_project/presentation/widgets/in_app_view.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:network_info_plus/network_info_plus.dart';

import '../../constants.dart';

/// HomePage for demo app to display wifi connection and displays error
class HomePage extends ConsumerStatefulWidget {
  final InAppView browser = InAppView();
  HomePage({
    super.key,
  });

  @override
  ConsumerState<HomePage> createState() => _HomePageState();
}

class _HomePageState extends ConsumerState<HomePage> {
  NetworkInfo info = NetworkInfo();
  bool? _stats = false;
  String? _text = 'None';

  ConnectivityResult _connectionStatus = ConnectivityResult.none;

  final Connectivity _connectivity = Connectivity();
  final ConnectivityResult _wifi = ConnectivityResult.wifi;
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;
  @override
  void initState() {
    super.initState();
    initConnectivity();
    // pass the subscription data from this value to a streamProvider and let other values listen to it.
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();

    super.dispose();
  }

  /// check for permission and connectivity of the device
  Future<void> initConnectivity() async {
    late ConnectivityResult result;

    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException {
      return;
    }
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  /// Update current connectivity status
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      _connectionStatus = result;
    });
    checkIpOfNetwork();
  }

  /// Check for wifi Ip address
  Future<void> checkIpOfNetwork() async {
    var value = await info.getWifiIP();
    setState(() {
      _text = value;
    });
  }

  /// option for webview calls
  var option = InAppBrowserClassOptions(
    crossPlatform: InAppBrowserOptions(
      toolbarTopBackgroundColor: Colors.white,
    ),
    inAppWebViewGroupOptions: InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        javaScriptEnabled: true,
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    if (_connectionStatus == _wifi && _text!.contains(phoneIp)) {
      setState(() {
        _stats = true;
      });
    } else {
      setState(() {
        _stats = false;
      });
    }
    const String title = 'Wifi Connected';
    const String noConnection = 'No connection check wifi';
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text('Agpro system'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              _stats! ? Icons.wifi : Icons.wifi_off,
              color: _stats! ? Colors.green : Colors.grey,
              size: MediaQuery.of(context).size.height * 0.2,
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              _stats! ? title : noConnection,
              style: GoogleFonts.inter(
                fontWeight: FontWeight.w500,
                fontSize: 20,
              ),
            ),
            TextButton(
              onPressed: () {},
              child: _stats! ? Text(_text!) : const CircularProgressIndicator(),
            ),
            ElevatedButton(
              onPressed: () {
                _stats!
                    ? widget.browser.openUrlRequest(
                        urlRequest: URLRequest(url: Uri.parse(url)),
                        options: option,
                      )
                    : alertDialog(context: context);
              },
              child: Text(
                _stats! ? 'Click to open site' : noConnection,
                style: TextStyle(color: _stats! ? Colors.black : Colors.grey),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

final wifiState = Provider.family<bool, bool>((ref, wifi) {
  if (wifi = true) {
    return true;
  }
  return false;
});

// final wifiValue 