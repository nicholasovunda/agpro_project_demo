import 'dart:developer';

import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class InAppView extends InAppBrowser {
  @override
  Future onBrowserCreated() async {
    log("Browser Created!");
  }

  @override
  Future onLoadStart(url) async {
    log("Started $url");
  }

  @override
  Future onLoadStop(url) async {
    log("Stopped $url");
  }

  @override
  void onLoadError(url, code, message) {
    log("Can't load $url.. Error: $message");
  }

  @override
  void onProgressChanged(progress) {
    log("Progress: $progress");
  }

  @override
  void onExit() {
    log("Browser closed!");
  }
}
