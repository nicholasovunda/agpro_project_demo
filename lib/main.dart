import 'package:agpro_project/presentation/widgets/layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'dart:io';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();

  /// in app browser android setup
  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  }
  runApp(const ProviderScope(child: MyApp()));
}
// TODO write the socket connection for the app using flutter binding
// TODO apply the StreamProvider to monitor the state of the connection at every point.
// TODO create a dialog alert box for when network goes off
// TODO use riverpod to monitor the stream for the wifi connectivity and connects to alert box for very thing

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomePage());
  }
}
